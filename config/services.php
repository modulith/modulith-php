<?php

declare(strict_types=1);

use Modulith\ModulithPhp\Core\Port\TemplateEngine\TemplateEngine;
use Modulith\ModulithPhp\Infrastructure\Framework\Symfony\CliApplication;
use Modulith\ModulithPhp\Infrastructure\TemplateEngine\Twig\TwigEngine;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services()
        ->defaults()
        ->autowire()
        ->autoconfigure();

    $services->bind('string $cwd', param('working_directory'));

    $services->instanceof(Command::class)->tag(CliApplication::COMMANDS_TAG);

    $services->load('Modulith\\ModulithPhp\\', __ROOT__ . '/src');

    $services->set(TemplateEngine::class, TwigEngine::class);

    $services->set(CliApplication::class)
        ->arg('$commands', tagged_iterator(CliApplication::COMMANDS_TAG))
        ->public();
};
