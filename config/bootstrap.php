<?php

declare(strict_types=1);

const __ROOT__ = __DIR__ . '/..';

require __ROOT__ . '/vendor/autoload.php';
