<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\TestFramework;

use Modulith\ModulithPhp\Infrastructure\Framework\Symfony\ApplicationFactory;
use Modulith\ModulithPhp\Infrastructure\Framework\Symfony\CliApplication;
use Symfony\Component\Console\Tester\CommandTester;

final readonly class TestApp
{
    private CliApplication $cliApp;

    public function __construct(string $cwd = __ROOT__ . '/var')
    {
        $containerBuilder = ApplicationFactory::initializeContainerBuilder();
        $containerBuilder->setParameter('working_directory', $cwd);

        $this->cliApp = ApplicationFactory::createApplication(ApplicationFactory::compileContainer($containerBuilder));
    }

    /**
     * @param string $commandName e.g.: 'app:create-user'
     * @param array<string, string|bool> $arguments e.g: ['username' => 'Wouter', '--some-option' => 'value, '--another-option' => true]
     * @param array<string> $inputs The inputs given by the user in interactive mode
     * @param array<string> $options Execution options [interactive, decorated, verbosity, capture_stderr_separately]
     *      capture_stderr_separately: Make output of stdOut and stdErr separately available
     *
     * @return string The output to the console
     */
    public function runCliCommand(
        string $commandName,
        array $arguments = [],
        array $inputs = [],
        array $options = []
    ): string {
        $commandTester = new CommandTester($this->cliApp->find($commandName));
        $commandTester->setInputs($inputs);
        $commandTester->execute($arguments, $options);

        return $commandTester->getDisplay();
    }
}
