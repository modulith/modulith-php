<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\TestFramework;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use const __ROOT__;
use const DIRECTORY_SEPARATOR;
use const GLOB_MARK;

abstract class AbstractTestCase extends TestCase
{
    protected static function setupTestCwd(string $methodFqcn): string
    {
        $cwd = __ROOT__ . '/var/test/' . self::extractCanonicalMethodName($methodFqcn);
        self::deleteDir($cwd);
        mkdir($cwd, 0o777, true);

        /** @var string */
        return realpath($cwd);
    }

    protected static function extractCanonicalMethodName(string $methodFqcn): string
    {
        return mb_substr($methodFqcn, mb_strrpos($methodFqcn, '::') + 2);
    }

    public static function deleteDir(string $dirPath): void
    {
        $dirPath = rtrim($dirPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        if (!file_exists($dirPath)) {
            return;
        }

        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("The given path is not a directory: '$dirPath'");
        }

        /** @var array<string> $fileList */
        $fileList = glob($dirPath . '*', GLOB_MARK);
        foreach ($fileList as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }

        rmdir($dirPath);
    }
}
