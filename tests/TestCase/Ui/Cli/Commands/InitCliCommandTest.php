<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\TestCase\Ui\Cli\Commands;

use Modulith\ModulithPhp\Core\Component\Config\Config;
use Modulith\ModulithPhp\Test\TestFramework\AbstractTestCase;
use Modulith\ModulithPhp\Test\TestFramework\TestApp;
use Modulith\ModulithPhp\Ui\Cli\Commands\InitCliCommand;
use PHPUnit\Framework\Attributes\Small;
use PHPUnit\Framework\Attributes\Test;

use function file_put_contents;

/**
 * @internal
 *
 * @small
 */
final class InitCliCommandTest extends AbstractTestCase
{
    /**
     * @test
     */
    public function it_creates_the_default_config(): void
    {
        $cwd = self::setupTestCwd(__METHOD__);
        $expectedConfigFilePath = $cwd . '/modulith.php';
        $expectedBaseNamespace = 'MyVendor\MyProject';
        $app = new TestApp($cwd);

        $app->runCliCommand(InitCliCommand::COMMAND_NAME);

        self::assertFileExists($expectedConfigFilePath);
        /** @var Config $config */
        $config = require $expectedConfigFilePath;
        self::assertEquals($expectedBaseNamespace, $config->srcNamespace);
        self::assertEquals($expectedBaseNamespace . '\Test', $config->testsNamespace);
        self::assertEquals($cwd . '/src', $config->srcPath);
        self::assertEquals($cwd . '/tests', $config->testsPath);
        self::deleteDir($cwd);
    }

    /**
     * @test
     */
    public function it_creates_the_default_config_in_the_specified_path(): void
    {
        $cwd = self::setupTestCwd(__METHOD__);
        $expectedConfigFilePath = $cwd . '/another-path/modulith.php';
        $expectedBaseNamespace = 'MyVendor\MyProject';
        $app = new TestApp($cwd);

        $app->runCliCommand(InitCliCommand::COMMAND_NAME, ['-f' => $expectedConfigFilePath]);

        self::assertFileExists($expectedConfigFilePath);
        /** @var Config $config */
        $config = require $expectedConfigFilePath;
        self::assertEquals($expectedBaseNamespace, $config->srcNamespace);
        self::assertEquals($expectedBaseNamespace . '\Test', $config->testsNamespace);
        self::assertEquals($cwd . '/src', $config->srcPath);
        self::assertEquals($cwd . '/tests', $config->testsPath);
        self::deleteDir($cwd);
    }

    /**
     * @test
     */
    public function it_creates_the_default_config_and_scaffolding(): void
    {
        $cwd = self::setupTestCwd(__METHOD__);
        $expectedConfigFilePath = $cwd . '/modulith.php';
        $app = new TestApp($cwd);

        $app->runCliCommand(InitCliCommand::COMMAND_NAME, ['-s' => true]);

        self::assertFileExists($expectedConfigFilePath);
        self::assertDirectoryExists($cwd);
        self::assertDirectoryExists($cwd . '/src/Core');
        self::assertDirectoryExists($cwd . '/src/Infrastructure');
        self::assertDirectoryExists($cwd . '/src/Ui');
        self::deleteDir($cwd);
    }

    /**
     * @test
     */
    public function it_creates_the_default_config_and_overwrites_existing(): void
    {
        $cwd = self::setupTestCwd(__METHOD__);
        $expectedConfigFilePath = $cwd . '/modulith.php';
        $app = new TestApp($cwd);
        $fileContents = 'aaa';
        file_put_contents($expectedConfigFilePath, $fileContents);

        $app->runCliCommand(InitCliCommand::COMMAND_NAME, ['-o' => true]);

        self::assertFileExists($expectedConfigFilePath);
        self::assertStringNotEqualsFile($expectedConfigFilePath, $fileContents);
        self::deleteDir($cwd);
    }

    /**
     * @test
     */
    public function it_creates_the_default_config_and_doesnt_overwrite_existing(): void
    {
        $cwd = self::setupTestCwd(__METHOD__);
        $expectedConfigFilePath = $cwd . '/modulith.php';
        $app = new TestApp($cwd);
        $fileContents = 'aaa';
        file_put_contents($expectedConfigFilePath, $fileContents);

        $app->runCliCommand(InitCliCommand::COMMAND_NAME);

        self::assertFileExists($expectedConfigFilePath);
        self::assertStringEqualsFile($expectedConfigFilePath, $fileContents);
        self::deleteDir($cwd);
    }

    /**
     * @test
     */
    public function it_creates_the_default_config_with_given_namespace(): void
    {
        $cwd = self::setupTestCwd(__METHOD__);
        $expectedConfigFilePath = $cwd . '/modulith.php';
        $expectedBaseNamespace = 'MyOtherVendor\MyOtherProject';
        $app = new TestApp($cwd);

        $app->runCliCommand(InitCliCommand::COMMAND_NAME, ['-a' => $expectedBaseNamespace]);

        self::assertFileExists($expectedConfigFilePath);
        /** @var Config $config */
        $config = require $expectedConfigFilePath;
        self::assertEquals($expectedBaseNamespace, $config->srcNamespace);
        self::assertEquals($expectedBaseNamespace . '\Test', $config->testsNamespace);
        self::assertEquals($cwd . '/src', $config->srcPath);
        self::assertEquals($cwd . '/tests', $config->testsPath);
        self::deleteDir($cwd);
    }
}
