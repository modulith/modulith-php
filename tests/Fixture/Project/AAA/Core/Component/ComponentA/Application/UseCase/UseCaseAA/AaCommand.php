<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentA\Application\UseCase\UseCaseAA;

use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\CommandBus\AsyncCommandInterface;

final readonly class AaCommand implements AsyncCommandInterface
{

}
