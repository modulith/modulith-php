<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentA\Application\UseCase\UseCaseAA;

use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentA\Domain\EntityAId;
use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\EventBus\EventInterface;

final readonly class AaEvent implements EventInterface
{
    public function __construct(public EntityAId $id)
    {
    }
}
