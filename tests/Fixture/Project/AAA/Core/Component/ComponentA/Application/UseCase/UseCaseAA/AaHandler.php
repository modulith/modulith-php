<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentA\Application\UseCase\UseCaseAA;

use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentA\Domain\EntityAId;
use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\EventBus\EventDispatcherInterface;

final readonly class AaHandler
{
    public function __construct(private EventDispatcherInterface $eventDispatcher)
    {
    }

    public function __invoke(AaCommand $command): void
    {
        $this->eventDispatcher->dispatch(new AaEvent(new EntityAId()));
    }
}
