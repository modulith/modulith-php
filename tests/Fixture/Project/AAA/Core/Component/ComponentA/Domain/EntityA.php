<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentA\Domain;

final readonly class EntityA
{
    public EntityAId $id;

    public function __construct()
    {
        $this->id = new EntityAId();
    }
}
