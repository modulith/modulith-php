<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentB\Application\Listener;

use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Component\ComponentA\Application\UseCase\UseCaseAA\AaEvent;
use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\EventBus\EventInterface;

final readonly class AaEventListener implements EventInterface
{
    public function doSomething(AaEvent $event): void
    {
    }
}
