<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\CommandBus;

/**
 * Each command handler should implement a handle(CommandClass $command) method
 * to handle the command specified by the handles() method.
 *
 * @method void __invoke(CommandInterface $command)
 */
interface CommandHandlerInterface
{
}
