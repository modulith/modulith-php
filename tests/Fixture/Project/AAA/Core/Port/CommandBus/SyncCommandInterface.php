<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\CommandBus;

interface SyncCommandInterface extends CommandInterface
{
}
