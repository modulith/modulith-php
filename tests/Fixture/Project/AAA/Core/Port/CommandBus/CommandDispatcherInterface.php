<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\CommandBus;

interface CommandDispatcherInterface
{
    public function dispatch(CommandInterface $command): void;
}
