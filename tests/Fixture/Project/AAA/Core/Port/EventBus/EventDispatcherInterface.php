<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\EventBus;

interface EventDispatcherInterface
{
    public function dispatch(EventInterface $event): void;
}
