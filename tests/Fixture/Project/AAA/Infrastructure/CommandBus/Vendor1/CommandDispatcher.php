<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Infrastructure\CommandBus\Vendor1;

use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\CommandBus\CommandDispatcherInterface;
use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\CommandBus\CommandInterface;

final readonly class CommandDispatcher implements CommandDispatcherInterface
{
    public function dispatch(CommandInterface $command): void
    {
    }
}
