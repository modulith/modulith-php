<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Test\Fixture\Project\AAA\Infrastructure\EventBus\Vendor1;

use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\EventBus\EventDispatcherInterface;
use Modulith\ModulithPhp\Test\Fixture\Project\AAA\Core\Port\EventBus\EventInterface;

final class EventDispatcher implements EventDispatcherInterface
{
    public function dispatch(EventInterface $event): void
    {
    }
}
