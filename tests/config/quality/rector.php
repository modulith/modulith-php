<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Array_\CallableThisArrayToAnonymousFunctionRector;
use Rector\CodeQuality\Rector\Identical\FlipTypeControlToUseExclusiveTypeRector;
use Rector\Config\RectorConfig;
use Rector\Core\ValueObject\PhpVersion;
use Rector\Php80\Rector\Class_\AnnotationToAttributeRector;
use Rector\Php81\Rector\Array_\FirstClassCallableRector;
use Rector\PHPUnit\Set\PHPUnitLevelSetList;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonyLevelSetList;
use Rector\Symfony\Symfony42\Rector\MethodCall\ContainerGetToConstructorInjectionRector;
use Rector\Symfony\Symfony53\Rector\Class_\CommandDescriptionToPropertyRector;

$rootDir = dirname(__FILE__, 4);

return static function (RectorConfig $rectorConfig) use ($rootDir): void {
    $rectorConfig->parallel();
    $rectorConfig->phpVersion(PhpVersion::PHP_82);
    $rectorConfig->cacheDirectory(__DIR__ . '/../../../var/cache/rector');
    $rectorConfig->paths([
        $rootDir . '/src',
        $rootDir . '/tests',
    ]);

    $rectorConfig->phpstanConfig(__DIR__ . '/phpstan.neon');

    $rectorConfig->skip([
        __DIR__ . '/var',
        __DIR__ . '/vendor',
        AnnotationToAttributeRector::class, // Because psalm fails to understand the #[test] attribute
        FlipTypeControlToUseExclusiveTypeRector::class,
        CallableThisArrayToAnonymousFunctionRector::class => [
            __DIR__ . '/config',
        ],
        FirstClassCallableRector::class => [
            __DIR__ . '/config',
        ],
        CommandDescriptionToPropertyRector::class,
        ContainerGetToConstructorInjectionRector::class => [
            __DIR__ . '/../../../src/Infrastructure/Framework/Symfony/ApplicationFactory.php',
        ],
    ]);

    $rectorConfig->sets([
        SetList::CODE_QUALITY,
        LevelSetList::UP_TO_PHP_82,
        SymfonyLevelSetList::UP_TO_SYMFONY_62,
        PHPUnitLevelSetList::UP_TO_PHPUNIT_100,
    ]);
};
