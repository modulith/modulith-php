<?php declare(strict_types=1);

$rootDir = dirname(__FILE__, 4);

$finder = (new PhpCsFixer\Finder())
    ->in([
        $rootDir . '/bin',
        $rootDir . '/build',
        $rootDir . '/config',
        $rootDir . '/src',
        $rootDir . '/tests/TestCase',
    ])
    ->name('*.php');

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules(require ".php-cs-fixer.rules.php")
    ->setIndent('    ')
    ->setLineEnding("\n")
    ->setFinder($finder)
    ->setUsingCache(true)
    ->setCacheFile($rootDir . '/var/cache/.php_cs.cache');
