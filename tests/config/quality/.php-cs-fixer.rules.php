<?php

return [
    '@Symfony' => true,
    '@PSR12' => true,
    '@PHP82Migration' => true,
    '@DoctrineAnnotation' => true,
    "align_multiline_comment" => ["comment_type" => "phpdocs_only"],
    'array_syntax' => ['syntax' => 'short'],
    'class_attributes_separation' => [
        'elements' => [
            "property" => "one",
            "method" => "one",
        ]
    ],
    'class_definition' => [
        'multi_line_extends_each_single_line' => true,
    ],
    'concat_space' => ['spacing' => 'one'],
    'final_internal_class' => true,
    'global_namespace_import' => [
        'import_constants' => true,
        'import_functions' => true,
        'import_classes' => true,
    ],
    'heredoc_indentation' => true,
    'increment_style' => [ 'style' => 'pre'],
    'method_chaining_indentation' => false,
    'multiline_whitespace_before_semicolons' => ['strategy' => 'no_multi_line'],
    'ordered_imports' => [
        'sort_algorithm' => 'alpha',
        'imports_order' => ['class', 'function', 'const'],
    ],
    'phpdoc_line_span' => [
        'const' => 'single',
        'property' => 'single',
        'method' => 'multi',
    ],
    'phpdoc_to_comment' => false, // because we want to use /** @psalm-suppress ... */
    'php_unit_internal_class' => true,
    'php_unit_method_casing' => ['case' => 'snake_case'],
    'php_unit_no_expectation_annotation' => true,
    'php_unit_set_up_tear_down_visibility' => true,
    'php_unit_size_class' => ['group' => 'small'],
    'php_unit_test_annotation' => ['style' => 'annotation'],
    'php_unit_test_case_static_method_calls' => ['call_type' => 'self'],
    'phpdoc_align' => false,
    'phpdoc_order' => true,
    'phpdoc_summary' => false,
    'echo_tag_syntax' => [
        'format' => 'long',
        'long_function' => 'echo',
        'shorten_simple_statements_only' => false,
    ],
    'no_superfluous_phpdoc_tags' => [
        'allow_mixed' => true,
    ],
    'no_useless_else' => true,
    'no_useless_return' => true,
    'return_type_declaration' => ['space_before' => 'none'],
    'single_line_throw' => false,
    'void_return' => true,
    'yoda_style' => [
        'equal' => false,
        'identical' => false,
    ],
];
