# Modulith / Modulith PHP

This is the repository of the CLI tool to work with ModulithPHP.

## Goals

- Enforce higher level architecture
  - Layers
    - Dependencies must go inwards & downwards
    - Core must not implement or extend from ports (allow for exceptions like commands and events)
    - Classes must only have sensible dependencies
  - Slices
    - Adapters must be decoupled
    - Core components must be decoupled
    - Core components tests must be decoupled
  - Code style
    - Classes must be suffixed
    - Classes must not be suffixed
  - Quality
    - Exceptions must inherit from project exception tree
    - Classes must not rely on reflection (there are exceptions like compiler passes)
    - Classes must be in map (ie a serialization map, so we are sure they can go ito a message queue)


- Generate schematics for the project
  - [C4 model (only 3 levels)](https://c4model.com/)
    - System Context diagram: Provides a starting point, showing how the software system in scope fits into the world around it.
    - Container diagram: Zooms into the software system in scope, showing the high-level technical building blocks.
    - Component diagram: Zooms into an individual container, showing the components inside it.
  - Application map [[1]](https://herbertograca.com/2019/08/12/documenting-software-architecture) [[2]](https://herbertograca.files.wordpress.com/2019/07/appmap00.png)


- Architecture forensics metrics
  - Hotspots detection (complexity * change_rate)
    - Prioritize code for refactoring
  - Code units temporal coupling
    - If there is no reason for them to change together (unlike tests and the code they test), they should probably be refactored
  - Code units ownership analysis
    - To detect team knowledge deficiencies
  - And plenty more...
    - Try to reuse [phpmetrics](https://phpmetrics.org/)


- Split a project per component
  - Analyse the monolithic project dependencies and create split versions per component so that they can be deployed independently as microservices
