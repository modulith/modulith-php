<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Ui\Cli;

use DateTimeImmutable;
use Hgraca\PhpExtension\DateTime\DateTimeException;
use Hgraca\PhpExtension\DateTime\DateTimeGenerator;
use Hgraca\PhpExtension\Helper\Assert;
use Hgraca\PhpExtension\Validator\Constraint\NumericValueConstraint;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractCliCommand extends Command
{
    final public const VALIDATE_SCALAR_ERROR_MSG = "The argument '%s' must be one of the following types: %s";
    final public const VALIDATE_DATE_ERROR_MSG = "The argument '%s' must be a valid date. Given value: '%s'";

    final public const OPTION_NAME_CONFIG_FILE = 'config-file';
    final public const OPTION_SHORTCUT_CONFIG_FILE = 'f';
    final public const OPTION_DESCRIPTION_CONFIG_FILE = 'The path to the config file';

    private const DEFAULT_CONFIG_FILE_NAME = 'modulith.php';

    /** @var ConsoleHelper */
    protected $consoleHelper;

    public function __construct(protected string $cwd, ConsoleHelper $consoleHelper = null)
    {
        parent::__construct();
        $this->consoleHelper = $consoleHelper ?? new ConsoleHelper();
        $this
            ->addOption(
                self::OPTION_NAME_CONFIG_FILE,
                self::OPTION_SHORTCUT_CONFIG_FILE,
                InputOption::VALUE_OPTIONAL,
                self::OPTION_DESCRIPTION_CONFIG_FILE,
                $cwd . '/' . self::DEFAULT_CONFIG_FILE_NAME,
            );
    }

    protected function writePerformanceInfo(OutputInterface $output, float $timeStart): void
    {
        $peakUsage = (memory_get_peak_usage() / 1024 / 1024) . ' MB';
        $this->consoleHelper->sayInfo($output, "Peak memory usage: $peakUsage");

        $timeEnd = microtime(true);
        $executionTime = round($timeEnd - $timeStart, 3);
        $executionTime = $executionTime >= 60 ? ($executionTime / 60) . ' mins' : $executionTime . 's';
        $this->consoleHelper->sayInfo($output, "Total Execution Time: $executionTime");
    }

    protected function extractIntegerFromGivenOption(InputInterface $input, string $integerOptionName): int
    {
        /** @var numeric $integerOption */
        $integerOption = $input->getOption($integerOptionName);
        Assert::that(
            $integerOption,
            (new NumericValueConstraint())
                ->withErrorMessage(sprintf(
                    self::VALIDATE_SCALAR_ERROR_MSG,
                    $integerOptionName,
                    'numeric'
                ))
        );

        return (int) $integerOption;
    }

    protected function extractStringFromGivenArgument(InputInterface $input, string $stringArgumentName): string
    {
        $stringArgument = $input->getArgument($stringArgumentName);
        Assert::isString($stringArgument, sprintf(self::VALIDATE_DATE_ERROR_MSG, $stringArgumentName, 'string'));

        return $stringArgument;
    }

    protected function extractStringFromGivenOption(InputInterface $input, string $stringOptionName): string
    {
        $stringOption = $input->getOption($stringOptionName);
        Assert::isString($stringOption, sprintf(self::VALIDATE_DATE_ERROR_MSG, $stringOptionName, 'string'));

        return $stringOption;
    }

    protected function extractDateFromGivenArgument(InputInterface $input, string $dateArgumentName): DateTimeImmutable
    {
        $dateArgument = $input->getArgument($dateArgumentName);
        Assert::isString($dateArgument, sprintf(self::VALIDATE_SCALAR_ERROR_MSG, $dateArgumentName, 'string'));

        return $this->extractDateFromString($dateArgument, $dateArgumentName);
    }

    protected function extractDateFromGivenOption(InputInterface $input, string $dateOptionName): DateTimeImmutable
    {
        $dateOption = $input->getOption($dateOptionName);
        Assert::isString($dateOption, sprintf(self::VALIDATE_SCALAR_ERROR_MSG, $dateOptionName, 'string'));

        return $this->extractDateFromString($dateOption, $dateOptionName);
    }

    /**
     * @return string[]
     */
    protected function extractStringArrayFromCommaSeparatedOption(InputInterface $input, string $optionName): array
    {
        $commaSeparatedString = $this->extractStringFromGivenOption($input, $optionName);

        return explode(',', $commaSeparatedString);
    }

    protected function getMemoryLimitInBytes(): int
    {
        $memoryLimit = ini_get('memory_limit');
        if (preg_match('/^(\d+)(.)$/', $memoryLimit, $matches) !== false) {
            $value = (int) $matches[1];
            switch ($matches[2]) {
                case 'G':
                    return $value * 1024 * 1024 * 1024;
                case 'M':
                    return $value * 1024 * 1024;
                case 'K':
                    return $value * 1024;
            }
        }

        return (int) $memoryLimit;
    }

    protected function bytesToMegabytes(int $bytes): int
    {
        return (int) ($bytes / 1024 / 1024);
    }

    private function extractDateFromString(string $dateString, string $argumentName): DateTimeImmutable
    {
        try {
            return DateTimeGenerator::generate($dateString);
        } catch (DateTimeException) {
            throw new InvalidArgumentException(
                sprintf(self::VALIDATE_DATE_ERROR_MSG, $argumentName, $dateString)
            );
        }
    }
}
