<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Ui\Cli\Commands;

use Modulith\ModulithPhp\Core\Component\Config\ConfigAlreadyExistsException;
use Modulith\ModulithPhp\Core\Component\Config\ConfigService;
use Modulith\ModulithPhp\Core\Component\Config\ScaffoldingService;
use Modulith\ModulithPhp\Ui\Cli\AbstractCliCommand;
use Modulith\ModulithPhp\Ui\Cli\ConsoleHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @psalm-api
 */
final class InitCliCommand extends AbstractCliCommand
{
    public const COMMAND_NAME = 'init';
    private const COMMAND_DESCRIPTION = 'Setup config, and optionally the scaffolding and demo code';

    private const OPTION_NAME_SCAFFOLDING = 'scaffolding';
    private const OPTION_SHORTCUT_SCAFFOLDING = 's';
    private const OPTION_DESCRIPTION_SCAFFOLDING = 'Create the project scaffolding';

    private const OPTION_NAME_OVERWRITE_CONFIG = 'overwrite';
    private const OPTION_SHORTCUT_OVERWRITE_CONFIG = 'o';
    private const OPTION_DESCRIPTION_OVERWRITE_CONFIG = 'If configuration exists, overwrite it';

    private const OPTION_NAME_BASE_NAMESPACE_CONFIG = 'namespace';
    private const OPTION_SHORTCUT_BASE_NAMESPACE_CONFIG = 'a';
    private const OPTION_DESCRIPTION_BASE_NAMESPACE_CONFIG = 'The main base namespace of the project, in format "MyVendor\MyProject", in between quotes';

    private const SUCCESS_MESSAGE = 'Finished without errors!';

    /** @psalm-suppress PropertyNotSetInConstructor It is set in the interact method */
    private string $namespace;

    public function __construct(
        string $cwd,
        ConsoleHelper $consoleHelper,
        private readonly ConfigService $configService,
        private readonly ScaffoldingService $scaffoldingService,
    ) {
        parent::__construct($cwd, $consoleHelper);
    }

    protected function configure(): void
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription(self::COMMAND_DESCRIPTION)
            ->addOption(
                self::OPTION_NAME_SCAFFOLDING,
                self::OPTION_SHORTCUT_SCAFFOLDING,
                InputOption::VALUE_NONE,
                self::OPTION_DESCRIPTION_SCAFFOLDING
            )
            ->addOption(
                self::OPTION_NAME_OVERWRITE_CONFIG,
                self::OPTION_SHORTCUT_OVERWRITE_CONFIG,
                InputOption::VALUE_NONE,
                self::OPTION_DESCRIPTION_OVERWRITE_CONFIG
            )
            ->addOption(
                self::OPTION_NAME_BASE_NAMESPACE_CONFIG,
                self::OPTION_SHORTCUT_BASE_NAMESPACE_CONFIG,
                InputOption::VALUE_REQUIRED,
                self::OPTION_DESCRIPTION_BASE_NAMESPACE_CONFIG
            );
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        /** @var string|null $namespaceOption */
        $namespaceOption = $input->getOption(self::OPTION_NAME_BASE_NAMESPACE_CONFIG);
        $this->namespace = $namespaceOption
            ?? $this->consoleHelper->ask(
                $input,
                $output,
                'What is the main application namespace (ie "MyVendor\MyProject")?',
                'MyVendor\MyProject'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $optionConfigFile */
        $optionConfigFile = $input->getOption(self::OPTION_NAME_CONFIG_FILE);
        $this->createConfigFile(
            $optionConfigFile,
            (bool) $input->getOption(self::OPTION_NAME_OVERWRITE_CONFIG),
            $this->namespace,
            $output
        );

        if ($input->getOption(self::OPTION_NAME_SCAFFOLDING) === true) {
            $this->createScaffolding($output);
        }

        $this->consoleHelper->sayInfo($output, self::SUCCESS_MESSAGE);

        return ConsoleHelper::EXIT_CODE_SUCCESS;
    }

    private function createConfigFile(
        string $configFilePath,
        bool $overwriteConfig,
        string $namespace,
        OutputInterface $output
    ): void {
        try {
            $this->configService->createConfigFile($configFilePath, $namespace, $overwriteConfig);
        } catch (ConfigAlreadyExistsException) {
            $this->consoleHelper->sayWarning($output, 'A file already exists in ' . $configFilePath . '. Skipping...');

            return;
        }

        $this->consoleHelper->sayInfo($output, 'Created the config file in ' . $configFilePath);
    }

    private function createScaffolding(OutputInterface $output): void
    {
        $this->scaffoldingService->createExplicitScaffolding();

        $this->consoleHelper->sayInfo($output, 'Created the scaffolding');
    }
}
