<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Core\Component\Config;

final readonly class ScaffoldingService
{
    private const EXPLICIT_SCAFFOLDING = [
        'Core' => [
            'Component' => [
                'ComponentX' => [
                    'Application' => [],
                    'Domain' => [],
                ],
                'ComponentY' => [
                    'Application' => [],
                    'Domain' => [],
                ],
                'ComponentZ' => [
                    'Application' => [],
                    'Domain' => [],
                ],
            ],
            'Port' => [
                'ToolA' => [],
                'ToolB' => [],
            ],
        ],
        'Infrastructure' => [
            'ToolA' => [
                'AdapterM' => [],
            ],
            'ToolB' => [
                'AdapterN' => [],
                'AdapterO' => [],
            ],
        ],
        'Ui' => [
            'Api' => [
                'V1' => [],
            ],
            'Backoffice' => [],
            'FrontStore' => [],
            'Cli' => [],
        ],
    ];

    public function __construct(private string $cwd)
    {
    }

    public function createExplicitScaffolding(): void
    {
        $this->createFoldersFromArray(self::EXPLICIT_SCAFFOLDING, $this->cwd . '/src');
    }

    /**
     * @param array<string, array<string, array<string, array<string, array<string>>>>> $array
     */
    private function createFoldersFromArray(array $array, string $basePath = ''): void
    {
        foreach ($array as $key => $value) {
            $directoryName = preg_replace('/[^a-z0-9-_]/i', '_', $key);
            $directoryPath = $basePath . '/' . $directoryName;

            if (!is_dir($directoryPath)) {
                mkdir($directoryPath, 0o755, true);
            }

            /** @psalm-suppress RedundantConditionGivenDocblockType */
            if (is_array($value)) {
                /**
                 * @psalm-suppress InvalidArgument
                 *
                 * @phpstan-ignore-next-line "Parameter #1 $array of method createFoldersFromArray() expects..."
                 */
                $this->createFoldersFromArray($value, $directoryPath);
            }
        }
    }
}
