<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Core\Component\Config;

use Exception;

final class ConfigAlreadyExistsException extends Exception
{
}
