<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Core\Component\Config;

use Modulith\ModulithPhp\Core\Port\TemplateEngine\TemplateEngine;

use function dirname;
use function file_exists;
use function file_put_contents;
use function is_dir;
use function mkdir;
use function preg_quote;
use function str_repeat;

final readonly class ConfigService
{
    public function __construct(private TemplateEngine $templateEngine, private string $cwd)
    {
    }

    public function createConfigFile(string $configFilePath, string $namespace, bool $overwriteConfig): void
    {
        if (!$overwriteConfig && file_exists($configFilePath)) {
            throw new ConfigAlreadyExistsException();
        }

        if (!is_dir(dirname($configFilePath))) {
            mkdir(dirname($configFilePath), 0o777, true);
        }

        file_put_contents(
            $configFilePath,
            $this->templateEngine->render(
                'config.php.twig',
                [
                    'navigationToRootPath' => $this->computePathFromConfigToRoot($this->cwd, $configFilePath),
                    'baseNamespace' => $namespace,
                ]
            ),
        );
    }

    private function computePathFromConfigToRoot(string $cwd, string $configFilePath): string
    {
        /** @phpstan-ignore-next-line "Parameter #1 $string of function trim expects string, string|null given" */
        $relativePathFromRootToConfigFile = trim(preg_replace('@^' . preg_quote($cwd) . '@', '', $configFilePath), '/');
        $depthLevels = count(explode('/', $relativePathFromRootToConfigFile)) - 1;

        return str_repeat('../', $depthLevels);
    }
}
