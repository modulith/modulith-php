<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Core\Component\Config;

use Arkitect\ClassSet;
use Arkitect\Expression\Expression;

final readonly class Config
{
    /**
     * @param Expression $phpOverlay Code that is quite assetic, we consider it as a thin layer above PHP itself, and depend on it as of PHP itself
     * @param Expression $mappers Code that is kind of config, namely mappers which are part of an adapter but need to depend directly on the core
     * @param Expression $conformistDependencies List of external namespaces we choose to conform with, maybe Doctrine?
     * @param Expression $sharedKernel For example, events and IDs are part of the shared kernel, because they are allowed to cross component boundaries
     */
    public function __construct(
        public string $srcPath,
        public string $testsPath,
        public string $srcNamespace,
        public string $testsNamespace,
        public Expression $phpOverlay,
        public Expression $mappers,
        public Expression $conformistDependencies,
        public Expression $domain,
        public Expression $application,
        public Expression $sharedKernel,
        public Expression $port,
        public Expression $adapter,
        public Expression $presentation,
        public Expression $controllers,
        public Expression $testCases,
        public Expression $vendor,
        public string $adaptersBaseNamespace,
        public string $adaptersBasePath,
    ) {
    }

    public function getClassSet(): ClassSet
    {
        return ClassSet::fromDir($this->srcPath, $this->testsPath);
    }
}
