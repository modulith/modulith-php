<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Core\Component\Config;

use Arkitect\Expression\Boolean\Andx;
use Arkitect\Expression\Boolean\Not;
use Arkitect\Expression\Expression;
use Arkitect\Expression\ForClasses\NotResideInTheseNamespaces;
use Arkitect\Expression\ForClasses\ResideInOneOfTheseNamespaces;
use Modulith\ModulithPhp\Infrastructure\Architecture\ArchCheck\Expression\NeverPassesExpression;

final class ExplicitConfigBuilder
{
    private readonly string $srcPath;

    private readonly string $testsPath;

    private string $testsNamespace;

    private Expression $phpOverlay;

    private Expression $domain;

    private Expression $application;

    private Expression $port;

    private Expression $adapter;

    private Expression $presentation;

    private Expression $testCases;

    private Expression $controllers;

    private Expression $mappers;

    private Expression $conformistDependencies;

    private Expression $sharedKernel;

    private Expression $vendor;

    private string $adaptersBaseNamespace;

    private string $adaptersBasePath;

    private function __construct(private readonly string $rootPath, private readonly string $baseNamespace)
    {
        $this->mappers = new NeverPassesExpression();
        $this->conformistDependencies = new NeverPassesExpression();
        $this->sharedKernel = new NeverPassesExpression();

        $this->srcPath = "$this->rootPath/src";
        $this->testsPath = "$this->rootPath/tests";
        $this->testsNamespace = "{$baseNamespace}\Test";
        $this->phpOverlay = new ResideInOneOfTheseNamespaces("{$baseNamespace}\PhpOverlay");
        $this->domain = new ResideInOneOfTheseNamespaces("{$this->baseNamespace}\Core\Component\*\Domain\*");
        $this->application = new ResideInOneOfTheseNamespaces("{$this->baseNamespace}\Core\Component\*\Application\*");
        $this->port = new ResideInOneOfTheseNamespaces("{$this->baseNamespace}\Core\Port\*", 'Psr\*');
        $this->adapter = new Andx(
            new ResideInOneOfTheseNamespaces("{$this->baseNamespace}\Infrastructure\*"),
            new Not($this->mappers),
        );
        $this->presentation = new ResideInOneOfTheseNamespaces("{$this->baseNamespace}\Ui\*");
        $this->controllers = new ResideInOneOfTheseNamespaces("{$this->baseNamespace}\Ui\Api\*");
        $this->testCases = new ResideInOneOfTheseNamespaces("{$this->testsNamespace}\TestCase\*");
        $this->vendor = new Andx(
            new NotResideInTheseNamespaces("{$this->baseNamespace}\*", "{$this->testsNamespace}\*"),
            new Not($this->conformistDependencies),
        );
        $this->adaptersBaseNamespace = "{$this->baseNamespace}\\Infrastructure";
        $this->adaptersBasePath = "{$this->rootPath}/src/Infrastructure";
    }

    public static function create(string $rootPath, string $baseNamespace): self
    {
        return new self($rootPath, $baseNamespace);
    }

    /**
     * @param string $testNamespace i.e. `MyVendor/MyProject/Test")`
     */
    public function withTestNamespace(string $testNamespace): self
    {
        $this->testsNamespace = $testNamespace;

        return $this;
    }

    /**
     * PhpOverlay refers to code that is assetic, not related to any particular domain and could be used
     * in any other project. In fact, it could even be part of PHP itself.
     */
    public function withPhpOverlay(Expression $phpOverlay): self
    {
        $this->phpOverlay = $phpOverlay;

        return $this;
    }

    public function withDomain(Expression $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function withApplication(Expression $application): self
    {
        $this->application = $application;

        return $this;
    }

    public function withPort(Expression $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function withAdapter(Expression $adapter): self
    {
        $this->adapter = $adapter;

        return $this;
    }

    public function withPresentation(Expression $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function withControllers(Expression $controllers): self
    {
        $this->controllers = $controllers;

        return $this;
    }

    public function withTestCases(Expression $testCases): self
    {
        $this->testCases = $testCases;

        return $this;
    }

    public function withVendor(Expression $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * @param Expression $mappers i.e. `new ResideInOneOfTheseNamespaces("{$srcNamespace}\Infrastructure\Persistence\Doctrine\Type\*")`
     */
    public function withMappers(Expression $mappers): self
    {
        $this->mappers = $mappers;

        return $this;
    }

    /**
     * @param Expression $sharedKernel i.e. new Andx(
     *   new Orx(
     *     new IsA(Event::class),
     *     new IsA(Id::class),
     *   ),
     *   new Not($conformistDependencies)
     * )
     */
    public function withSharedKernel(Expression $sharedKernel): self
    {
        $this->sharedKernel = $sharedKernel;

        return $this;
    }

    /**
     * @param Expression $conformistDependencies i.e. `new ResideInOneOfTheseNamespaces('Doctrine\\')`
     */
    public function withConformistDependencies(Expression $conformistDependencies): self
    {
        $this->conformistDependencies = $conformistDependencies;

        return $this;
    }

    public function withAdaptersBaseNamespace(string $adaptersBaseNamespace): self
    {
        $this->adaptersBaseNamespace = $adaptersBaseNamespace;

        return $this;
    }

    public function withAdaptersBasePath(string $adaptersBasePath): self
    {
        $this->adaptersBasePath = $adaptersBasePath;

        return $this;
    }

    public function build(): Config
    {
        return new Config(
            srcPath: $this->srcPath,
            testsPath: $this->testsPath,
            srcNamespace: $this->baseNamespace,
            testsNamespace: $this->testsNamespace,
            phpOverlay: $this->phpOverlay,
            mappers: $this->mappers,
            conformistDependencies: $this->conformistDependencies,
            domain: $this->domain,
            application: $this->application,
            sharedKernel: $this->sharedKernel,
            port: $this->port,
            adapter: $this->adapter,
            presentation: $this->presentation,
            controllers: $this->controllers,
            testCases: $this->testCases,
            vendor: $this->vendor,
            adaptersBaseNamespace: $this->adaptersBaseNamespace,
            adaptersBasePath: $this->adaptersBasePath,
        );
    }
}
