<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Core\Port\TemplateEngine;

/**
 * @phpstan-type TemplateData array<string, string|int|bool|array<string, string|int|bool>>
 */
interface TemplateEngine
{
    /**
     * @phpstan-param TemplateData $data
     */
    public function render(string $templateName, array $data): string;
}
