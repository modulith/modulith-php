<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Infrastructure\Framework\Symfony;

use Symfony\Component\Console\Application as SymfonyConsoleApplication;
use Symfony\Component\Console\Command\Command;

class CliApplication extends SymfonyConsoleApplication
{
    private const NAME = 'ModulithPHP';

    final public const COMMANDS_TAG = 'console.command';

    /**
     * @param iterable<Command> $commands
     */
    public function __construct(iterable $commands = [])
    {
        parent::__construct(self::NAME);

        foreach ($commands as $command) {
            $this->add($command);
        }
    }
}
