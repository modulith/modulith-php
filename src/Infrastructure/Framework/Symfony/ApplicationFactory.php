<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Infrastructure\Framework\Symfony;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

use const __ROOT__;

final readonly class ApplicationFactory
{
    public static function createApplication(Container $container = null): CliApplication
    {
        $container ??= self::compileContainer();

        /** @var CliApplication */
        return $container->get(CliApplication::class);
    }

    public static function compileContainer(ContainerBuilder $containerBuilder = null): Container
    {
        $containerBuilder ??= self::initializeContainerBuilder();

        $containerBuilder->compile(true);

        return $containerBuilder;
    }

    public static function initializeContainerBuilder(): ContainerBuilder
    {
        $containerBuilder = new ContainerBuilder();

        $containerBuilder->setParameter('application_directory', __ROOT__);
        /** @phpstan-ignore-next-line "Parameter #1 $string of function rtrim expects string, string|false given." */
        $containerBuilder->setParameter('working_directory', rtrim(getcwd(), '/'));

        $loader = new PhpFileLoader($containerBuilder, new FileLocator());
        $loader->load(__ROOT__ . '/config/services.php');

        return $containerBuilder;
    }
}
