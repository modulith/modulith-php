<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Infrastructure\TemplateEngine\Twig;

use Modulith\ModulithPhp\Core\Port\TemplateEngine\TemplateEngine;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

use const __ROOT__;

final readonly class TwigEngine implements TemplateEngine
{
    private Environment $twig;

    public function __construct()
    {
        $this->twig = new Environment(new FilesystemLoader(__ROOT__ . '/templates'));
    }

    public function render(string $templateName, array $data): string
    {
        return $this->twig->render($templateName, $data);
    }
}
