<?php

declare(strict_types=1);

namespace Modulith\ModulithPhp\Infrastructure\Architecture\ArchCheck\Expression;

use Arkitect\Analyzer\ClassDescription;
use Arkitect\Expression\Description;
use Arkitect\Expression\Expression;
use Arkitect\Expression\MergeableExpression;
use Arkitect\Rules\Violation;
use Arkitect\Rules\ViolationMessage;
use Arkitect\Rules\Violations;
use InvalidArgumentException;

final readonly class NeverPassesExpression implements Expression, MergeableExpression
{
    public function describe(ClassDescription $theClass, string $because = ''): Description
    {
        return new Description('always violated', $because);
    }

    public function evaluate(ClassDescription $theClass, Violations $violations, string $because = ''): void
    {
        $violations->add(
            Violation::create(
                $theClass->getFQCN(),
                ViolationMessage::selfExplanatory($this->describe($theClass, $because))
            )
        );
    }

    public function mergeWith(Expression $expression): Expression
    {
        if (!$expression instanceof self) {
            throw new InvalidArgumentException('Can not merge expressions. The given expression should be of type ' . self::class . ' but is of type ' . $expression::class);
        }

        return new self();
    }
}
